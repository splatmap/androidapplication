package co.splatmap.splatmapapp.splatmap;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.hardware.Camera;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CameraActivity extends ActionBarActivity implements SensorEventListener {

    CameraPreview mCameraPreview;
    Camera mCamera;
    LocationManager mLocationManager;
    private LocationListener mLocationListener;
    private Location mLocation;
    private SensorManager mSensorManager;
    private Sensor mAcclerometer;
    private Sensor mMagnetometer;
    private Float azimuth;
    private Float pitch;
    private Float roll;


    public void takeCameraPicture(View view){

        if((mCameraPreview) == null ||(mCamera == null)){
            return;
        }
        mCamera.takePicture(mShutter, null, mPicture);
//        return the camera view to the preview.



    }

    private Camera.ShutterCallback mShutter = new Camera.ShutterCallback(){

        @Override
        public void onShutter() {
            Log.d("INFO","Shutter Callback Called");
//            final MediaPlayer mPlayer = MediaPlayer.create(CameraActivity.this, android.R.raw.splat);
//            mPlayer.start();
        }
    };

    private Camera.PictureCallback mPicture = new Camera.PictureCallback(){

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null){
                Log.d("INFO", "Error creating media file, check storage permissions");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                Log.d("INFO","Location at time of photo is:"+mLocation.toString());
            } catch (FileNotFoundException e) {
                Log.d("INFO", "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d("INFO", "Error accessing file: " + e.getMessage());
            }
            camera.startPreview();
        }
    };

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        mCamera = getCameraInstance();
        mLocationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        registerLocationSensors();
        getLocationListener();
        mCameraPreview = new CameraPreview(this.getApplicationContext(),mCamera);
        FrameLayout preview = (FrameLayout)findViewById(R.id.cameraframe);
        preview.addView(mCameraPreview);
        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,mLocationListener);
    }

    private void registerLocationSensors() {
        mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensorList = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        Log.d("INFO", "available sensor types are:" + sensorList);
        mAcclerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mSensorManager.registerListener(this,mAcclerometer,SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this,mMagnetometer,SensorManager.SENSOR_DELAY_UI);
    }

    private void getCurrentOrientation(){


    };

    private void getLocationListener() {
        LocationListener listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mLocation = location;
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        mLocationListener = listener;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_camera, menu);
        return true;
    }

    public static Camera getCameraInstance(){
        Camera camera = null;
        try {
            camera = Camera.open();
        }catch (Exception e){
            System.out.println("Error accessing camera");
        }
        return camera;
    }
    private void ReleaseCamera(){

        mCameraPreview = null;
        mCamera = null;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ReleaseCamera();
    }

    public void getOrientation(View view){
        Toast toast = Toast.makeText(getApplicationContext(),"Azimuth:"+azimuth.toString()+" Pitch:"+pitch.toString()+" Roll:"+roll.toString(), Toast.LENGTH_LONG);
        toast.show();
    };

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;
        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                azimuth = orientation[0]; // orientation contains: azimut, pitch and roll
                pitch = orientation[1];
                roll = orientation[2];
            }
        }
    }
    float[] mGravity;
    float[] mGeomagnetic;
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {


    }
}
