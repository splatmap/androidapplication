package co.splatmap.splatmapapp.splatmap;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Collections;
import java.util.LinkedList;

/**
 * Created by bryan on 10/08/2015.
 */
public class AccountManager {
    final String TAG = "AccountManager";

    final AccountInfo info;
    final CallbackManager facebookCallbackManager;
    final LinkedList<Callbacks> listeners;

    private static AccountManager instance = null;

    private AccountManager(final Activity parentActivity) {
        this.info = new AccountInfo();
        this.facebookCallbackManager = CallbackManager.Factory.create();
        this.listeners = new LinkedList<>();

        final AccessToken facebookAccessToken = AccessToken.getCurrentAccessToken();
        if (facebookAccessToken != null) {
            Log.d(TAG, "facebook session is already active");

            this.info.type = LoginType.FACEBOOK;
            this.info.token = facebookAccessToken.getToken();
        }

        LoginManager.getInstance().registerCallback(this.facebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook login was successful");

                AccountManager.this.info.type = LoginType.FACEBOOK;
                AccountManager.this.info.token = AccessToken.getCurrentAccessToken().getToken();

                for (Callbacks listener : listeners) {
                    listener.onLoginSuccess(AccountManager.this.info);
                }
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook login has been cancelled");
            }

            @Override
            public void onError(FacebookException e) {
                Log.e(TAG, "a facebook exception happened", e);
            }
        });
    }

    public String getToken() { return this.info.token; }

    public boolean isLoggedIn() {
        return this.info.isAvailable();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.facebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void logInWithFacebook(final Activity parentActivity) {
        LoginManager.getInstance().logInWithReadPermissions(parentActivity, Collections.<String>emptyList());
    }

    public void registerCallbacks(final Callbacks listener) {
        this.listeners.push(listener);
    }

    public static AccountManager getInstance() {
        return instance;
    }

    public static void initialize(final Activity parentActivity) {
        instance = new AccountManager(parentActivity);
    }

    public interface Callbacks {
        void onLoginSuccess(AccountInfo info);
    }

    public enum LoginType {
        FACEBOOK
    }

    public class AccountInfo {
        private LoginType type;
        private String token;

        private AccountInfo() {
            this.type = null;
            this.token = null;
        }

        boolean isAvailable() {
            return this.type != null;
        }

        public LoginType type() {
           return this.type;
        }

        public String token() {
            return this.token;
        }
    }
}
